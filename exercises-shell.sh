1 devez entrer “man mkdir”

Man mkdir entre User Commands

2.	Vous devez créer 3 répertoires “rep-toto”, “rep-titi”, “rep-tata” en imbriqué.

$ mkdir rep-toto rep-titi rep-tata
$ ls
all-new  rep-tata  rep-titi  rep-toto  test4

3 vous devez créer 3 fichiers “toto”, “titi”, “tata” dans chacun des répertoires que vous venez de créer.
 $ cd rep-tata

 ~/rep-tata ⌚ 15:56:32
 $ touch toto.txt tata.txt titi.txt

 ~/rep-tata ⌚ 15:57:04
 $ ls
 tata.txt  titi.txt  toto.txt

 ~/rep-tata ⌚ 15:57:06
 $ cd ../

 ~ ⌚ 15:57:22
 $ cd rep-toto

 ~/rep-toto ⌚ 15:57:36
 $ touch toto.txt tata.txt titi.txt

 ~/rep-toto ⌚ 15:58:04
 $ ls
 tata.txt  titi.txt  toto.txt

 ~/rep-toto ⌚ 15:58:06
 $ cd ../

 ~ ⌚ 15:58:13
 $ cd rep-titi

 ~/rep-titi ⌚ 15:58:24
 $ touch toto.txt tata.txt titi.txt

 ~/rep-titi ⌚ 15:58:43
 $ ls
 tata.txt  titi.txt  toto.txt
4 Vous devez créer un symlink portant le nom de 42 pour accéder au répertoire rep-toto que vous venez de créer.

 ln -s [fichier cible] [Nom de fichier symbolique]

 $ ln -s rep-toto 42

 $ ls 42
 lrwxrwxrwx 1 lea  lea     42 -> rep-toto
5 Vous devez lister les informations des répertoires avec une commande vous permettant de voir les fichiers cachés et permettre une lecture humaine du poid de chaque fichier.

 $ ls -al  or $ ls -a -l

 total 272
6 vous devez installer “oh-my-zsh”.

 sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
7 vous devez installer le template “robbyrussell” avec “oh-my-zsh”.

 $ vim .zshrc ensuite changer le zsh- theme  “ robbyrussell ”. 
 Escape + :wq

